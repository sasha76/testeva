<?php


class EventsController
{

    public function __construct()
    {
        global $DB;
        $this->db = $DB;
    }

    public function getEvents($data)
    {
        $where = '';
        if (isset($data['start']) && isset($data['end'])) {
            $start = date('Y-m-d', $data['start']);
            $end = date('Y-m-d', $data['end']);
            $where = " WHERE date_start Between '$start' AND '$end'";
        }
        $err_mess = "<br>Function: getEvents<br>Line: ";
        $sql = "SELECT date_start as start, date_end as `end`, title, description, id FROM events $where";
        $res = $this->db->Query($sql, false, $err_mess . __LINE__);

        return $res->fetchAll();
    }

    public function addEvent($data)
    {
        if ($this->validateForm($data)) {
            $err_mess = "<br>Function: addEvent<br>Line: ";
            $sql = "INSERT INTO `events` (`title`, `description`, `date_start`, `date_end`) 
                    VALUES ('{$data['title']}', '{$data['description']}', '{$data['date_start']}', '{$data['date_end']}');";
            $this->db->Query($sql, false, $err_mess . __LINE__);

            return true;
        }
        return false;
    }

    public function validateForm($data)
    {
        if ((isset($data['title']) && trim($data['title']) == '')
            || (isset($data['date_start']) && trim($data['date_start']) == '')) {
            return false;
        }
        return true;
    }
}