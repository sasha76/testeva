<?
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');
?>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#calendar').fullCalendar({
                firstDay: 1,
                height: 200,
                eventLimit: 3,
                eventTextColor: 'white',
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: ''
                },
                monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Иοюнь', 'Иοюль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                monthNamesShort: ['Янв.', 'Фев.', 'Март', 'Апр.', 'Май', 'Иοюнь', 'Иοюль', 'Авг.', 'Сент.', 'Окт.', 'Ноя.', 'Дек.'],
                dayNames: ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"],
                dayNamesShort: ["ВС", "ПН", "ВТ", "СР", "ЧТ", "ПТ", "СБ"],
                buttonText: {
                    prev: "&nbsp;&#9668;&nbsp;",
                    next: "&nbsp;&#9658;&nbsp;",
                    prevYear: "&nbsp;&lt;&lt;&nbsp;",
                    nextYear: "&nbsp;&gt;&gt;&nbsp;",
                    today: "Сегодня",
                    month: "Месяц",
                    week: "Неделя",
                    day: "День"
                },
                eventSources: [
                    {
                        url: '/ajax.handler.php?action=getEvents',
                    }
                ],
                eventClick: function (event) {
                    if (event.description)
                        alert(event.description);
                    else
                        alert('Нет описания');
                },
            });

            $("#addEvent").click(function () {
                var $form = $('#formEvent');

                $.ajax({
                    url: 'ajax.handler.php',
                    type: 'POST',
                    dataType: "html",
                    data: $form.serialize(),
                    success: function (res) {
                        if (res !== 'false') {
                            let mas = {
                                title: $('#title').val(),
                                start: $('#date_start').val(),
                                end: $('#date_end').val(),
                                description: $('#description').val()
                            };
                            $('#calendar').fullCalendar('renderEvent', mas);
                        } else {
                            alert('Введите корректные данные');
                        }
                    },
                    error: function () {
                        console.log('error');
                    }
                });
            });
        });

    </script>
    <div id="calendar"></div>
    <div id="dialog">
        <p>Добавить Заметку</p>
        <form method="post" id="formEvent">
            <input type="hidden" name="action" value="addEvent">
            <div class="form-group">
                <label for="title">Title*</label>
                <input type="text" class="form-control" id="title" name="title" placeholder="title">
            </div>
            <div class="form-group">
                <label for="description">Description (Описание открывается по клику на заметку)</label>
                <textarea name="description" id="description" class="form-control" rows="2"
                          placeholder="description"></textarea>
            </div>
            <div class="form-group">
                <label for="date_start">Date start*</label>
                <input type="date" class="form-control datepicker" id="date_start" name="date_start"
                       placeholder="date_start">
            </div>
            <div class="form-group">
                <label for="date_end">Date end</label>
                <input type="date" class="form-control datepicker" id="date_end" name="date_end" placeholder="date_end">
            </div>
            <button type="button" name="add" id="addEvent" class="btn btn-success">Add</button>
        </form>
    </div>
<?
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');
?>