<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<!DOCTYPE html>
<html>
	<head>
		<?$APPLICATION->ShowHead();?>
		<title><?$APPLICATION->ShowTitle();?></title>
		<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

        <link rel='stylesheet' type='text/css' href='<?=SITE_TEMPLATE_PATH?>/assets/js/jqfc/fullcalendar.css'/>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>

        <script type='text/javascript' src='<?=SITE_TEMPLATE_PATH?>/assets/js/jqfc/fullcalendar.min.js'></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
              integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
        <style>
            .fc-sat .fc-day-number, .fc-sun .fc-day-number, .fc-sat.fc-widget-header, .fc-sun.fc-widget-header {
                color: #c3000e;
            }
            #dialog {
                width: 500px;
                margin: 0 auto;
                margin-top: 30px;
            }
            .fc-event {
                cursor: pointer;
            }
        </style>
	</head>
	<body>
    <?$APPLICATION->ShowPanel();?>