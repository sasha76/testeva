<? require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");

if (!isset($_REQUEST['action']) && $_REQUEST['action'] == '') {
    return false;
}

require_once 'handlers/EventsController.php';

$events = new EventsController();

switch ($_REQUEST['action']) {
    case 'getEvents':
        $resource = $events->getEvents($_REQUEST);
        break;
    case 'addEvent':
        $resource = $events->addEvent($_REQUEST);
        break;
}

echo json_encode($resource);